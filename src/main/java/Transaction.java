public class Transaction extends Thread {

    private final Bank bank;
    private final String from;
    private final String to;
    private final long amount;

    public Transaction(Bank bank, String from, String to, long amount) {
        this.bank = bank;
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    @Override
    public void run() {
            bank.transfer(from, to, amount);
    }
}
