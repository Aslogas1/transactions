import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<String, Account> accountMap = new HashMap<>();
        accountMap.put("1", new Account(3_000_000, "1"));
        accountMap.put("2", new Account(3_000_000, "2"));
        accountMap.put("3", new Account(3_000_000, "3"));
        accountMap.put("4", new Account(3_000_000, "4"));
        accountMap.put("5", new Account(3_000_000, "5"));
        accountMap.put("6", new Account(3_000_000, "6"));
        Bank bank = new Bank(accountMap);

            Transaction transaction1 = new Transaction(bank, "1", "2", 40_000);
            transaction1.start();
            Transaction transaction2 = new Transaction(bank, "3", "4", 50_000);
            transaction2.start();
            Transaction transaction3 = new Transaction(bank, "5", "6", 60_000);
            transaction3.start();
    }
}
