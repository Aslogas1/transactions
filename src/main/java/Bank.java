import java.util.Map;
import java.util.Random;

public class Bank {

    private final Map<String, Account> accounts;
    private final Random random = new Random();

    public Bank(Map<String, Account> accounts) {
        this.accounts = accounts;
    }

    public synchronized boolean isFraud()
            throws InterruptedException {
        Thread.sleep(1000);
        return random.nextInt(100) < 5;
    }

    public synchronized void transfer(String fromAccountNum, String toAccountNum, long amount) {
        while (accounts.get(fromAccountNum).getMoney() > amount) {
            try {
                if(amount > 50000){
                    isFraud();
                }
                if(isFraud()){
                    System.out.println(Thread.currentThread().getName() + " Fraud!");
                    wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long from = getBalance(fromAccountNum) > amount ?
                    getBalance(fromAccountNum) - amount : getBalance(fromAccountNum);
            accounts.get(fromAccountNum).setMoney(from);

            long to = getBalance(toAccountNum) + amount;
            accounts.get(toAccountNum).setMoney(to);
            System.out.println(accounts.get(fromAccountNum).getAccNumber() + " account remaining money:"
                    + accounts.get(fromAccountNum).getMoney());
            System.out.println(accounts.get(toAccountNum).getAccNumber() + " To account remaining money:"
                    + accounts.get(toAccountNum).getMoney());
        }
    }

    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */
    public long getBalance(String accountNum) {
        long result = 0;
        for (Account account : accounts.values()) {
            if (account.getAccNumber().equals(accountNum)) {
                result = account.getMoney();
            }
        }
        return result;
    }

    public long getSumAllAccounts() {
        long sum = 0;
        for (Account account : accounts.values()) {
            sum += account.getMoney();
        }
        return sum;
    }

    public Map<String, Account> getAccounts() {
        return accounts;
    }
}
