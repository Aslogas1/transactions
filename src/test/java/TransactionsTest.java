import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

public class TransactionsTest {

    private final Map<String, Account> accountMap = Map.of(
      "1", new Account(100_000, "1"),
      "2", new Account(200_000, "2"),
      "3", new Account(300_000, "3")
    );

    @Test
    void getBalance() {
        assertEquals(100_000, accountMap.get("1").getMoney());
    }

    @Test
    void getSumAllAccounts() {
        long money = 0;
        for (Account account : accountMap.values()) {
            money += account.getMoney();
        }
        assertEquals(600_000, money);

    }
}
